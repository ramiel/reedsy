const JobController = (jobHandler) => ({

  create: (req, res) => {
    jobHandler.add(req.body)
    .then(job => res.json(job))
    .catch(error => res.json({error}))
  },

  list: (req, res) => {
    jobHandler.list()
    .then(jobs => res.json(jobs))
    .catch(error => res.json({error}))
  }
})

module.exports = JobController
