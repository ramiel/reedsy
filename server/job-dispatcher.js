const rabbit = require('amqplib')
const EventEmitter = require('events')

class JobDispatcher extends EventEmitter {
  constructor ({uri, communicationQueue}) {
    super()
    this._connection = null
    this._channel = null
    this._communicationQueue = communicationQueue
    this._respondingQueueName = null
    this._uri = uri
  }

  _connect () {
    if (this._connection !== null) {
      return Promise.resolve(this._connection)
    }

    return rabbit.connect(this._uri, null)
      .then(connection => {
        this._connection = connection
        return connection
      })
  }

  _createChannel () {
    return this._connect()
      .then(connection => connection.createChannel())
      .then(channel => {
        this._channel = channel
        return this._channel
      })
  }

  _processResponse (message) {
    return new Promise((resolve, reject) => {
      try {
        const job = JSON.parse(message.content.toString())
        if (job.error) {
          return reject(job.err)
        }
        resolve(job)
      } catch (e) {
        reject(e)
      }
    })
  }

  init () {
    return this._createChannel()
      .then(channel => {
        const {name, options} = this._communicationQueue
        return Promise.all([
          channel.assertQueue('', {exclusive: true}),
          channel.assertQueue(name, options)
        ])
      })
      .then(([respondingQueue]) => {
        this._respondingQueueName = respondingQueue.queue

        this._channel.consume(respondingQueue.queue, message => {
          this._processResponse(message)
            .then(job => this.emit('step', job))
            .catch(e => this.emit('error', {id: message.properties.correlationId}))
        }, {noAck: true})
      })
  }

  dispatch (job) {
    if (this.connection === null) {
      return Promise.reject(new Error('Must init before'))
    }
    try {
      const message = new Buffer(JSON.stringify(job))
      const options = {
        correlationId: job.id,
        replyTo: this._respondingQueueName,
        priority: job.priority || 0
      }
      const sent = this._channel.sendToQueue(
        this._communicationQueue.name,
        message,
        options
      )
      return Promise.resolve(sent)
    } catch (e) {
      return Promise.reject(e)
    }
  }
}

module.exports = JobDispatcher
