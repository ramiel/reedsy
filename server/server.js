const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const HttpServer = require('http').createServer
const path = require('path')
const JobController = require('./controllers/job-controller')

const Server = ({config, jobHandler}) => {
  const jobController = JobController(jobHandler)
  const {port} = config
  const app = express()
  const http = HttpServer(app)

  app.use(cors())
  app.use(bodyParser.json())
  app.use(bodyParser.urlencoded({ extended: false }))

  app.post('/jobs', jobController.create)
  app.get('/jobs', jobController.list)
  app.use('/', express.static(path.join(__dirname, '..', 'client', 'dist')))

  return {
    http,
    start: () => new Promise((resolve, reject) => {
      http.listen(port, err => {
        if (err) {
          return reject(err)
        }
        resolve(app)
      })
    })
  }
}

module.exports = Server
