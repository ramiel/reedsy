const config = require('config')
const Server = require('./server')
const SocketServer = require('./socket-server')
const JobHandler = require('./job-handler')
const JobDispatcher = require('./job-dispatcher')

const rabbitConfig = config.get('rabbit')
const httpConfig = config.get('server')

const jobDispatcher = new JobDispatcher(rabbitConfig)
const jobHandler = new JobHandler(jobDispatcher)
const httpServer = Server({config: httpConfig, jobHandler})

jobDispatcher.init()
  .then(() => SocketServer({http: httpServer.http, jobHandler}))
  .then(() => httpServer.start())
  .then(() => {
    console.log(`Reedsy app listening on *:${httpConfig.port}`)
  })
  .catch(err => {
    console.error(err)
    process.exit(1)
  })
