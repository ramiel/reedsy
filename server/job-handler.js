const {JobFactory, JOB_STATUS} = require('./job-factory')
const EventEmitter = require('events')

const savedJobs = {}

class JobHandler extends EventEmitter {
  constructor (jobDispatcher) {
    super()
    this.jobDispatcher = jobDispatcher
    this.jobDispatcher.on('step', this._onJobStep.bind(this))
    this.jobDispatcher.on('error', this._onJobErrored.bind(this))
  }

  _onJobStep (job) {
    this.update(job)
      .then(job => {
        this.emit('job:step', job)
      })
  }

  _onJobErrored (error) {
    this.get(error.id)
      .then(job => {
        job.status = JOB_STATUS.ERRORED
        return this.update(job)
      })
      .then(job => this.emit('job:error', job))
  }

  add (job) {
    job = JobFactory.create(job)
    savedJobs[job.id] = job
    return this.jobDispatcher.dispatch(job)
      .then(running => job)
      .catch(e => {
        job.status = JOB_STATUS.ERRORED
        return this.update(job)
      })
  }

  update (job) {
    savedJobs[job.id] = job
    return Promise.resolve(job)
  }

  get (id) {
    return Promise.resolve(savedJobs[id] || null)
  }

  list () {
    return Promise.resolve(
      Object.keys(savedJobs)
        .map(k => savedJobs[k])
    )
  }
}

module.exports = JobHandler
