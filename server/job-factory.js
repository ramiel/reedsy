const uuid = require('uuid/v4')

const JOB_TYPES = {
  PDF: 'PDF',
  HTML: 'HTML'
}

const JOB_STATUS = {
  QUEUED: 'QUEUED',
  RUNNING: 'RUNNING',
  SUCCEDED: 'SUCCEDED',
  ERRORED: 'ERRORED'
}

const JOB_PRIORITY = {
  HTML: 1,
  PDF: 0
}

const isValidType = type => Object.keys(JOB_TYPES).indexOf(type) !== -1

const JobFactory = {
  create: ({name, type, data = {}}) => {
    if (!isValidType(type)) {
      throw new Error('Invalid type')
    }
    const id = uuid()
    return Object.assign(
      {
        id,
        name: `${type} conversion ${id}`,
        created: new Date(),
        status: JOB_STATUS.QUEUED
      },
      {
        name,
        type,
        data,
        status: JOB_STATUS.QUEUED,
        priority: JOB_PRIORITY[type] || 0
      }
    )
  }
}

module.exports = {
  JobFactory,
  JOB_TYPES,
  JOB_STATUS,
  JOB_PRIORITY
}
