const Socket = require('socket.io')

const SocketServer = ({http, jobHandler}) => {
  this.jobHandler = jobHandler
  this.socket = Socket(http)
  this.jobHandler.on('job:step', job => this.socket.emit('job:step', job))
  this.jobHandler.on('job:error', job => this.socket.emit('job:error', job))
}

module.exports = SocketServer
