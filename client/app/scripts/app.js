'use strict'

/**
 * @ngdoc overview
 * @name reedsyApp
 * @description
 * # reedsyApp
 *
 * Main module of the application.
 */
angular
  .module('reedsyApp', [
    'ngResource',
    'ui-notification'
  ])
  .constant('config', {
    apiUrl: '//localhost:9000'
  })
