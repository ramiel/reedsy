'use strict'

/**
 * @ngdoc function
 * @name reedsyApp.controller:JobsCtrl
 * @description
 * # JobsCtrl
 * Controller of the reedsyApp
 */
angular.module('reedsyApp')
  .controller('JobsCtrl', ['Job', 'JobFactory', 'JobSocket', 'Notification',
    function (Job, JobFactory, JobSocket, Notification) {
      this.jobs = Job.query()

      var onJobChanged = function (job) {
        var index = _.findIndex(this.jobs, {id: job.id})
        if (index !== -1) {
          this.jobs[index] = Object.assign({}, this.jobs[index], job)
        }
      }

      this.createJob = function (type) {
        const job = new Job(JobFactory.create(type))

        job.$save().then(
          function (job) {
            this.jobs.push(job)
          }.bind(this),
          function (err) {
            Notification.error({
              title: 'Oops',
              message: 'An error occurred communicating with the server. Is it running?',
              delay: 2000
            })
            console.err(err)
          })
      }

      JobSocket.onJobStep(onJobChanged.bind(this))

      JobSocket.onJobError(onJobChanged.bind(this))
    }
  ])
