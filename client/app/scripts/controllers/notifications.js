'use strict'

/**
 * @ngdoc function
 * @name reedsyApp.controller:NotificationsCtrl
 * @description
 * # NotificationsCtrl
 * Controller of the reedsyApp
 */
angular.module('reedsyApp')
  .controller('NotificationsCtrl', ['JobSocket', 'Notification', function (JobSocket, Notification) {
    var onNotification = function (job) {
      switch (job.status) {
        case 'SUCCEDED':
          Notification.success({
            title: 'Hooray!',
            message: 'The job ' + job.name + ' has succeded to convert!',
            delay: 2000
          })
          break
        case 'ERRORED':
          Notification.error({
            title: 'Oops',
            message: 'The job ' + job.name + ' has failed to convert!',
            delay: 2000
          })
          break
        default:
          break
      }
    }

    JobSocket.onJobStep(onNotification.bind(this))

    JobSocket.onJobError(onNotification.bind(this))
  }])
