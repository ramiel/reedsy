'use strict'

/**
 * @ngdoc service
 * @name reedsyApp.Job
 * @description
 * # Job
 * Factory in the reedsyApp.
 */
angular.module('reedsyApp').factory('Job', ['$resource', 'config', function ($resource, config) {
  return $resource(config.apiUrl + '/jobs/:id')
}])
