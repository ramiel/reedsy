'use strict'

/**
 * @ngdoc service
 * @name reedsyApp.JobSocket
 * @description
 * # JobSocket
 * Factory in the reedsyApp.
 */
angular.module('reedsyApp')
  .factory('JobSocket', ['$rootScope', 'config', function ($rootScope, config) {
    var socket = io(config.apiUrl)

    return {
      onJobStep: function (callback) {
        socket.on('job:step', function (job) {
          $rootScope.$apply(function () {
            callback(job)
          })
        })
      },

      onJobError: function (callback) {
        socket.on('job:error', function (job) {
          $rootScope.$apply(function () {
            callback(job)
          })
        })
      }
    }
  }])
