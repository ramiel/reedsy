'use strict'

/**
 * @ngdoc service
 * @name reedsyApp.HtmlJob
 * @description
 * # HtmlJob
 * Factory in the reedsyApp.
 */
angular.module('reedsyApp')
  .factory('JobFactory', function () {
    var counters = {
      HTML: 0,
      PDF: 0
    }

    // Public API here
    return {
      create: function (type, data) {
        type = type ? type.toUpperCase() : 'HTML'
        data = data || {}
        return {
          name: type + ' #' + (++counters[type]),
          type: type,
          data: data
        }
      }
    }
  })
