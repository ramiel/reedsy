const rabbit = require('amqplib')
const {JOB_STATUS} = require('../server/job-factory')

class Worker {
  constructor (rabbit, options) {
    const {uri, communicationQueue} = rabbit
    const {delay, parallelProcessing} = options
    this._connection = null
    this._channel = null
    this._communicationQueue = communicationQueue
    this._respondingQueueName = null
    this._uri = uri
    this._channel = null
    this._delay = delay
    this._parallelProcessing = parallelProcessing
  }

  _parseMessage (msg) {
    return new Promise((resolve, reject) => {
      try {
        const job = JSON.parse(msg.content.toString())
        resolve(job)
      } catch (e) {
        reject(e)
      }
    })
  }

  _processJob (job) {
    return new Promise((resolve, reject) => {
      try {
        job = JSON.parse(job)
        const delay = this._delay[job.type] || 0
        console.log(`Processing ${job.type} job ${job.id}`)
        setTimeout(() => {
          job.status = JOB_STATUS.SUCCEDED
          resolve(job)
        }, delay)
      } catch (e) {
        reject(e)
      }
    })
  }

  _replyStatusChange (channel, msg, content) {
    channel.sendToQueue(
      msg.properties.replyTo,
      new Buffer(JSON.stringify(content)),
      {correlationId: msg.properties.correlationId}
    )
  }

  _prepare () {
    if (this._channel !== null) {
      return Promise.resolve(this._channel)
    }
    const {name, options} = this._communicationQueue
    return rabbit.connect(this._uri)
      .then(connection => connection.createChannel())
      .then(channel => {
        this._channel = channel
        channel.assertQueue(name, options)
        return channel
      })
  }

  do () {
    return this._prepare()
      .then(channel => {
        const {name} = this._communicationQueue

        channel.prefetch(this._parallelProcessing)

        channel.consume(name, (msg) => {
          this._parseMessage(msg)
          .then(job => {
            job.status = JOB_STATUS.RUNNING
            this._replyStatusChange(channel, msg, job)
            return this._processJob(msg.content.toString())
          })
          .then(job => {
            this._replyStatusChange(channel, msg, job)
            channel.ack(msg)
          })
          .catch(e => {
            const error = new Buffer(JSON.stringify({error: 'An error occurred'}))
            this._replyStatusChange(channel, msg, error)
          })
        })
      })
  }
}

module.exports = Worker
