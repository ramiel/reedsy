const cluster = require('cluster')
const config = require('config')
const Worker = require('./worker')

const workers = config.get('workers')

if (cluster.isMaster) {
  for (let i = 0; i < workers.number; i++) {
    cluster.fork()
  }
} else {
  const rabbit = config.get('rabbit')
  const simulation = config.get('simulation')
  const options = Object.assign({}, simulation, workers.options)
  const worker = new Worker(rabbit, options)
  worker.do()
    .then(() => console.log(`Started worker ${cluster.worker.id}`))
    .catch(err => console.error(err))
}
