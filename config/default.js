module.exports = {
  // This configuration can be overwritten in several ways
  // The quicker is to create a local.{js, json, yml} file
  // aside this one overwriting only the needed keys

  // Http server configuration
  server: {
    port: 9000
  },

  // RabbitMQ *minimal* configuration
  rabbit: {
    uri: 'amqp://localhost:5672',
    communicationQueue: {
      name: 'jobs',
      options: {
        durable: false,
        arguments: {
          // Minimum wll be 0, max is 5
          'x-max-priority': 5
        }
      }
    }
  },

  // Configuraiton for the convertion workers
  workers: {
    // How many workers are spawned
    number: require('os').cpus().length,

    options: {
      // How many conversion, each worker, will start concurrently
      // 1 means each worker will accept 1 conversion and it will wait until it's finished
      // before accepting another one
      // This value can be set to false to accept infinite jobs
      parallelProcessing: 1
    }
  },

  simulation: {
    // how much time any job should take per type
    delay: {
      HTML: 10 * 1000,
      PDF: 100 * 1000
    }
  }
}
