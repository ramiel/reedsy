# Reedsy challenge

This solution implements a micro-service approach to the problem. Considering that the conversion jobs can be (and actually they are) very time-consuming, I totally decoupled the API from the conversion service.
This approach let you scale separately the API and the conversion workers. I imagine in a production environment, you want, for instance, to spawn more conversion workers than API servers, or you can choose to have different hardware (or cloud machines) for the two services because converting a document needs more memory and cpu than serving a REST API. Several considerations can be done, which are well solved by this architecture:

- Different memory needs
- Different disk space needs
- Different scale policies
- It brings a more reliable service: if the conversion workers are down or busy, this has no impact on the http server which can continue to accept conversion requests

## Content

- `server`    
    It contains the http API server
- `conversion-service`    
    It contains the conversion workers
- `client`   
    Contains the angular application code

## Installation

This application needs [`RabbitMQ`](https://www.rabbitmq.com/)@>3.5.0 to work.
If you don't want to install it you can simply use docker-compose on the root of this project to have a rabbit instance

`docker-compose up -d`

This will only give you rabbitMQ, not the application.

For the application itself you only need

```bash
npm install
npm start
```

Now you can browse to [http://localhost:9000](http://localhost:9000)

For convenience this will start both the http server and the conversion service. If you want to launch them separately you can run

```bash
npm run start-http-server
npm run start-servant
```

Of course you can use `yarn` if you prefer.    
The dependencies are managed at project level. In a real world application the `http-server` and the `conversion-service` can be different projects

## Configuration

[`config`](https://github.com/lorenwest/node-config) has been used as configuration provider. If you want to play with the configuration the simplest is to duplicate `config/default.js` file into `config/local.js` (or .json or .yml) and override only the needed keys. For example there are parameters to change job timing.

## Code style

I don't know which code style you use and I'm not very opinionated on it. I just think it's important to choose a style and to follow it. If the style can be enforced it's better. For this I choosed `eslint` and I applied the [`standard`](https://standardjs.com/) code style.    
You can run the linter with    
`npm run lint` or `yarn lint`

## Client application

The client application is served by the same API server for convenience. It can be served alone of course. Pointing your browser to `http://localhost:9000` (or the port you configured), will serve the lasted build of the client application. I included it in the repo for convenience (it should not). It can be rebuilt

```bash
cd client
bower install
grunt
```

or can be served standalone with (the port is 9090 in that case)

```bash
grunt serve
```

The configuration for the client is stored in `client/app/scripts/app.js`

## What is missing

- **Tests**: I usually start projects using TDD. This time I did not and I ended up a project without tests. Shame on me!
- **Logging**: this project log rarely and using `console.log`. It is something I avoid usually. I did not pay attention to this detail in this case
- **Presistence**: Once the server start, it persists jobs in memory. I did it like this because I considered persistence a secondary target of this test. Restarting the http server results in job loss but some job can still be queued and processed in background. So if you try to add new jobs and their status is queued, it means something else is processing in backgroud. Have a look at the console. To reset the queue, the quicker is to act on rabbitMQ.


# Question 1

I report here what I wrote on the application letter which answers to this question

Hello! I've been happy this morning when I stumble upon this job offer not just because I love to read (which is pretty common) but because I've been always interested in book industry.
I'm a web developer since 2009 and I worked in Italy and France. In the latest years I focused on node.js and working on scalable API (rest api and graphql) developing and designing the infrastucture. In my latest job I helped to design a REST API written in nodejs (with hapi, using mongoDB and served through docker containers) which has to scale for a large number of user. One thing I loved to do, in my career, was to design and develop a healt realtime monitor system which is now used by real people in several hospitals. It's the more tangible project I worked on and I'm very proud of it.
On my spare time I love whatching movies (and tv series) or play music with my friends...if I'm not somewhere outside.

# Question 2

Operational transformation is a technology which aims to let people to collaborate, mainly in writings text.

Each person execute an operation on the shared resource and then each operation is executed after being modified considering the transformation introduced by the previous one.

The key point is that at the end any partecipant reachs the `same state` of the others.

A server take care of guarantee this.
Let's say we have two clients which are editing a document. The server keep two copies of the revisions which the client are modifing.
Let's say the two clients do this operations

Client 1 - Add a "C" at the beginning of the doc
Client 2 - Modify the client in the 2nd position "a" -> "b"

The server have to change the operations based on the others
The operation for the `client 1` doesn't change. A "C" will be added to thw beginning of the document. The operation for the `client 2` will become
`modify the character in the 3rd position`.

This is a basic version of opearational transformation.

# Question 3

I'll present two naive ideas on the subject.

The first approach which came on my mind is not to reinvent the wheel and to try to use what already exists: version control system like git or mercurial. They are already good at saving revisions and to access them quickly and to produce diff between them. Every time a user want to produce a tag it is represented by a tag entirely managed by the VCS. The diff produced by two tag can be easily used to show an interface like the one you put in the image. The downside of this solution is that this way we are actually using a VCS as a database which is not good. It's hard to scale or replicate or synchronize.

The second approach would be to use a database of your choice (whatever, it is not important) and to follow a model of reverse deltas. Basically the latest version will be always saved completely. When a new version is tagged this step have to be done:
1. Create a diff which take from the current to the previous tag
2. Replace the previous tag with this (reverse) diff
3. Save the latest tag entirely

The advantage here is that the most viewed version (the latest) has the fastest access,while to access an old revision we need to build all the previous diff. We are sacrifing speed in favor of space. This is not a real problem because, depending on the application, the use case "show me an old revision"  occur less than "show me the last one".
This solution can be implemented with any database and there are some which has revision systems already implemented (I'm thinking of Cassandra or ChouchBase).
